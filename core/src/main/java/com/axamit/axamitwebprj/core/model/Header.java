package com.axamit.axamitwebprj.core.model;


import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;

@Model(adaptables = Resource.class)
public class Header {

    @Inject
    private Resource items;

    @Inject
    private String logoImg;

    private ArrayList<Page> pages = new ArrayList<>();

    @PostConstruct
    private void init() {
        PageManager pageManager = items.getResourceResolver().adaptTo(PageManager.class);
        items.getChildren().forEach(resource ->
                pages.add(pageManager.getContainingPage(resource.getValueMap().get("url").toString())));

    }


    public String getLogoImg() {
        return logoImg;
    }

    public ArrayList<Page> getPages() {
        return pages;
    }

}