package com.axamit.axamitwebprj.core.model;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Carousel {

    @Inject
    private Resource slides;

    private ArrayList<ValueMap> slideList = new ArrayList<>();

    @PostConstruct
    private void init() {
        if (slides != null) {
            slides.getChildren().forEach(resource ->
                    slideList.add(resource.getValueMap())
            );
        }
    }

    public List getSlideList() {
        return slideList;
    }

}
