package com.axamit.axamitwebprj.core.model;


import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.*;

@Model(adaptables = Resource.class)
public class Footer {

    @Inject
    private Resource items;

    private List<Map> links = new ArrayList<>();

    private static final String URL_KEY = "url";
    private static final String URL_NAME_KEY = "urlName";

    @PostConstruct
    private void init() {
        items.getChildren().forEach(resource -> {
            ValueMap valueMap = resource.getValueMap();
            Map<String, String> map = new HashMap<>();
            map.put(URL_KEY, valueMap.get(URL_KEY).toString());
            map.put(URL_NAME_KEY, valueMap.get(URL_NAME_KEY).toString());
            links.add(map);
        });
    }

    public List<Map> getLinks() {
        return links;
    }
}